/* $Id$ */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <pi-source.h>
#include <pi-socket.h>
#include <pi-mail.h>
#include <pi-dlp.h>
#include <pi-version.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <utime.h>
#include <unistd.h>
#include <pwd.h>
#include <signal.h>

#include <gnome-pilot-conduit.h>
#include <gnome-pilot-conduit-standard.h>
#include "email_conduit.h"

#define CONDUIT_VERSION "0.10"

/*#define EC_DEBUG */
#ifdef EC_DEBUG
#define LOG(format,args...) g_log (G_LOG_DOMAIN, \
                                   G_LOG_LEVEL_MESSAGE, \
                                   "email: " format, ##args)
#else
#define LOG(format,args...)
#endif

#define EC_PAD 8

const char *EC_SEND_ACTION [] = {"delete", "file"};
const char *EC_RECV_ACTION [] = {"copy", "delete", "mirror"};

GnomePilotConduit *conduit_get_gpilot_conduit( guint32 pilotId ) ;
void conduit_destroy_gpilot_conduit( GnomePilotConduit *c );
static gboolean save_config (GKeyFile    *kfile, const gchar *conf);
static GKeyFile* get_kfile (const gchar *conf);
static void migrate_conf (const gchar *old, const gchar *new);
extern time_t parsedate(char *p);


static void 
load_configuration(ConduitCfg **c,guint32 pilotId) 
{
	gchar *iPilot;
	GKeyFile *kfile;
	GError *error = NULL;

	g_assert(c!=NULL);
	*c = g_new0(ConduitCfg,1);
	(*c)->child = -1;

 	kfile = get_kfile("email-conduit");
	iPilot = g_strdup_printf ("Pilot_%u", pilotId);

	(*c)->sendmail = g_key_file_get_string (kfile, iPilot, "sendmail", &error);
	if (error) {
	    g_warning (_("Unable load key email-conduit/%s/sendmail: %s"), iPilot, error->message);
	    g_error_free (error);
	    error = NULL;
	    (*c)->sendmail = g_strdup ("/usr/lib/sendmail -t -i");
	}
	(*c)->fromAddr = g_key_file_get_string (kfile, iPilot, "from_address", NULL);
	(*c)->sendAction = g_key_file_get_string (kfile, iPilot, "send_action", &error);
	if (error) {
	    g_warning (_("Unable load key email-conduit/%s/send_action: %s"), iPilot,
		error->message);
	    g_error_free (error);
	    error = NULL;
	    (*c)->sendAction = g_strdup ("file");
	}
	(*c)->mhDirectory = g_key_file_get_string (kfile, iPilot, "mh_directory", NULL);
	(*c)->mboxFile = g_key_file_get_string (kfile, iPilot, "mbox_file", NULL);
	(*c)->receiveAction = g_key_file_get_string (kfile, iPilot,
	    "receive_action", &error);
	if (error) {
	    g_warning (_("Unable load key email-conduit/%s/receive_action: %s"), iPilot,
		error->message);
	    g_error_free (error);
	    error = NULL;
	    (*c)->receiveAction = g_strdup ("copy");
	}


	(*c)->pilotId = pilotId;

	g_free (iPilot);
	g_key_file_free (kfile);
}

static void
save_configuration(ConduitCfg *c)
{
	gchar *iPilot;
	GKeyFile *kfile;

	g_assert(c!=NULL);

 	kfile = get_kfile("email-conduit");
	iPilot = g_strdup_printf ("Pilot_%u", c->pilotId);

	if (c->sendmail != NULL)
	    g_key_file_set_string (kfile, iPilot, "sendmail", c->sendmail);
	else
	    g_key_file_remove_key (kfile, iPilot, "sendmail", NULL);
	if (c->fromAddr != NULL)
	    g_key_file_set_string (kfile, iPilot, "from_address", c->fromAddr);
	else
	    g_key_file_remove_key (kfile, iPilot, "from_addres", NULL);
	g_key_file_set_string (kfile, iPilot, "send_action", c->sendAction);
	if (c->mhDirectory != NULL)
	    g_key_file_set_string (kfile, iPilot, "mh_directory", c->mhDirectory);
	else
	    g_key_file_remove_key (kfile, iPilot, "mh_directory", NULL);
	if (c->mboxFile != NULL)
	    g_key_file_set_string (kfile, iPilot, "mbox_file", c->mboxFile);
	else
	    g_key_file_remove_key (kfile, iPilot, "mbox_file", NULL);
	g_key_file_set_string (kfile, iPilot, "receive_action", c->receiveAction);

	g_free(iPilot);
	save_config (kfile, "email-conduit");
}

static void 
copy_configuration(ConduitCfg *d, ConduitCfg *c)
{
        g_return_if_fail(c!=NULL);
        g_return_if_fail(d!=NULL);

	/* it is always safe to free NULL pointers with [g_]free */
	g_free(d->sendmail);
	g_free(d->fromAddr);
	g_free(d->sendAction);
	g_free(d->mhDirectory);
	g_free(d->mboxFile);
	g_free(d->receiveAction);

	d->sendmail      = g_strdup(c->sendmail);
	d->fromAddr      = g_strdup(c->fromAddr);
	d->sendAction    = g_strdup(c->sendAction);
	d->mhDirectory   = g_strdup(c->mhDirectory);
	d->mboxFile      = g_strdup(c->mboxFile);
	d->receiveAction = g_strdup(c->receiveAction);

	d->pilotId       = c->pilotId;
}

static ConduitCfg*
dupe_configuration(ConduitCfg *c) 
{
	ConduitCfg *d;
	g_return_val_if_fail(c!=NULL,NULL);
	d = g_new0(ConduitCfg,1);
	copy_configuration(d,c);
	return d;
}

/** this method frees all data from the conduit config */
static void 
destroy_configuration(ConduitCfg **c) 
{
	g_assert(c!=NULL);
	g_assert(*c!=NULL);
	g_free( (*c)->sendmail );
	g_free( (*c)->fromAddr );
	g_free( (*c)->sendAction );
	g_free( (*c)->mhDirectory );
	g_free( (*c)->mboxFile );
	g_free( (*c)->receiveAction );
	g_free(*c);
	*c = NULL;
}

void markline( unsigned char *msg ) 
{
    while( (*msg) != '\n' && (*msg) != 0 ) {
        msg++; 
    }
    (*msg) = 0;
}

int openmhmsg( char *dir, int num ) 
{ 
    char filename[1000];
    
    sprintf( filename, "%s/%d", dir, num ); 
    return( open( filename, O_RDONLY ) );
}

char *skipspace( char *c ) 
{
    while ( c && ((*c == ' ') || (*c == '\t')) ) { 
        c++;
    }
    return( c );
}

void header( struct Mail *m, char *t )
{
    static char holding[4096];

    if ( t && strlen(t) && ( t[strlen(t)-1] == '\n' ) ) {
        t[strlen(t)-1] = 0;
    }
         
    if ( t && ((t[0] == ' ') || (t[0] == '\t')) ) { 
        if ( (strlen(t) + strlen(holding)) > 4096 ) { 
            return; /* Just discard approximate overflow */
        }
        strcat( holding, t+1 ); 
        return; 
    }
           
    /* Decide on what we do with m->sendTo */
           
    if ( strncmp( holding, "From:", 5 ) == 0 ) { 
        m->from = strdup( skipspace( holding + 5 ) ); 
    } else if ( strncmp( holding, "To:", 3 ) == 0 ) { 
        m->to = strdup( skipspace( holding + 3 ) ); 
    } else if ( strncmp( holding, "Subject:", 8 ) == 0 ) { 
        m->subject = strdup( skipspace( holding + 8 ) ); 
    } else if ( strncmp( holding, "Cc:", 3 ) == 0 ) {
        m->cc = strdup( skipspace( holding + 3 ) ); 
    } else if ( strncmp( holding, "Bcc:", 4 ) == 0 ) { 
        m->bcc = strdup( skipspace( holding + 4 ) ); 
    } else if ( strncmp( holding, "Reply-To:", 9 ) == 0 ) { 
        m->replyTo = strdup( skipspace( holding + 9 ) ); 
    } else if ( strncmp( holding, "Date:", 4 ) == 0 ) { 
        time_t d = parsedate(skipspace(holding+5)); 
        
        if ( d != -1 ) { 
            struct tm * d2; 
            
            m->dated = 1; 
            d2 = localtime( &d ); 
            m->date = *d2; 
        } 
    } 
    
    holding[0] = 0; 
    
    if ( t ) { 
        strcpy( holding, t );
    }
}

/* helper function to identify identical e-mails */
static gint match_mail(gconstpointer a, gconstpointer b)
{
    MailDBRecord *c = (MailDBRecord *) a;
    MailDBRecord *d = (MailDBRecord *) b;

    LOG("matching records [%d vs. %d]", c->size, d->size);
    if (c->size != d->size) {
	return 1;
    }

    return memcmp(c->buffer, d->buffer, c->size);
}

static gboolean
write_message_to_pilot (GnomePilotConduit *c, GnomePilotDBInfo *dbi, 
			int dbHandle, unsigned char *buffer, int msg_num)
{
    unsigned char *msg;
    int h;
    struct Mail t;
    int len;
    GList *inbox_list;
    GList *field;
    MailDBRecord needle;
    
    t.to = NULL;
    t.from = NULL;
    t.cc = NULL;
    t.bcc = NULL;
    t.subject = NULL;
    t.replyTo = NULL;
    t.sentTo = NULL;
    t.body = NULL;
    t.dated = 0;

    /* initialise these to something */
    t.read = 0;
    t.signature = 0;
    t.confirmRead = 0;
    t.confirmDelivery = 0;
    t.priority = 0;
    t.addressing = 0;

    msg = buffer;
    h = 1;
    
    while ( h == 1 ) {
	markline( msg );
	
	if ( ( msg[0] == 0 ) && ( msg[1] == 0 ) ) {
	    break;
	}
	
	if ( msg[0] == 0 ) {
	    h = 0;
	    header( &t, 0 );
	} else {
	    header( &t, (char *) msg );
	}
	msg += strlen((char *)msg)+1;
    }
    
    if ( (*msg) == 0 ) {
	h = 1;
    }
    
    if ( h ) {
	fprintf( stderr, "Incomplete message %d\n", msg_num );
	free_Mail( &t );
	return FALSE;
    }
    
    t.body = strdup( (char *) msg );
    
    len = pack_Mail( &t, buffer, 0xffff );

    /* if this mail already exists in the Palms inbox then skip this mail */
    needle.size = len;
    needle.buffer = buffer;
    inbox_list = (GList*) g_object_get_data(G_OBJECT(c), "inbox_list");
    field = g_list_find_custom(inbox_list, &needle, match_mail);
    if (field) {
    	/* remove the mail from the list as we've already seen it */
	inbox_list = g_list_remove_link(inbox_list, field);
	g_object_set_data(G_OBJECT(c),"inbox_list",(gpointer)inbox_list);
	free(field->data);
	g_list_free_1(field);
	LOG("Skipping message (already on Palm device)");
	return TRUE;
    }
    
    if ( dlp_WriteRecord( dbi->pilot_socket, dbHandle, 0, 0, 0, buffer,
			  len, 0 ) > 0 ) {
	return TRUE;
    } else {
	fprintf( stderr, "Error writing message to Pilot\n" );
	return FALSE;
    }
}

static gint synchronize( GnomePilotConduit *c, GnomePilotDBInfo *dbi ) 
{
    int dbHandle;
#ifdef PILOT_LINK_0_12
    pi_buffer_t *pi_buf;
#else
    guchar buffer[0xffff];
#endif
    struct MailAppInfo tai;
    struct MailSyncPref pref;
    struct MailSignaturePref sig;
    int i;
    int rec;
    int dupe;
    GList *inbox_list;
   
    g_message ("SendMail Conduit v %s",CONDUIT_VERSION);

    memset( &tai, '\0', sizeof( struct MailAppInfo ) );

    if ( dlp_OpenDB( dbi->pilot_socket, 0, 0x80|0x40, "MailDB", 
                     &dbHandle ) < 0 ) {
        fprintf( stderr, "Unable to open mail database\n" );
        return( -1 );
    }

#ifdef PILOT_LINK_0_12
    pi_buf = pi_buffer_new (0xffff);    
    dlp_ReadAppBlock( dbi->pilot_socket, dbHandle, 0, 0xffff, pi_buf);
    unpack_MailAppInfo( &tai, pi_buf->data, 0xffff );
#else
    dlp_ReadAppBlock( dbi->pilot_socket, dbHandle, 0, buffer, 0xffff );
    unpack_MailAppInfo( &tai, buffer, 0xffff );
#endif
   
    pref.syncType = 0;
    pref.getHigh = 0;
    pref.getContaining = 0;
    pref.truncate = 8 * 1024;
    pref.filterTo = 0;
    pref.filterFrom = 0;
    pref.filterSubject = 0;

#ifdef PILOT_LINK_0_12
     if ( pi_version( dbi->pilot_socket ) > 0x0100 ) {
         if ( dlp_ReadAppPreference( dbi->pilot_socket, makelong("mail"), 1, 1, 
                                    pi_buf->allocated, pi_buf->data, 0, 0 ) >= 0 ) {
            unpack_MailSyncPref( &pref, pi_buf->data, pi_buf->allocated );
         } else { 
             if ( dlp_ReadAppPreference( dbi->pilot_socket, makelong("mail"), 1,
                                        1, pi_buf->allocated, pi_buf->data, 0, 0 ) >= 0 ) { 
                unpack_MailSyncPref( &pref, pi_buf->data, pi_buf->allocated); 
             } else {
 	      LOG("Couldn't get any mail preferences.\n",0);
             }
         } 
 
          if ( dlp_ReadAppPreference( dbi->pilot_socket, makelong("mail"), 3, 1, 
                                    pi_buf->allocated, pi_buf->data, 0, 0 ) > 0 ) {
            unpack_MailSignaturePref( &sig, pi_buf->data, pi_buf->allocated);
         } 
     }
#else
    if ( pi_version( dbi->pilot_socket ) > 0x0100 ) {
        if ( dlp_ReadAppPreference( dbi->pilot_socket, makelong("mail"), 1, 1, 
                                    0xffff, buffer, 0, 0 ) >= 0 ) {
            unpack_MailSyncPref( &pref, buffer, 0xffff );
        } else { 
            if ( dlp_ReadAppPreference( dbi->pilot_socket, makelong("mail"), 1,
                                        1, 0xffff, buffer, 0, 0 ) >= 0 ) { 
                unpack_MailSyncPref( &pref, buffer, 0xffff ); 
            } else {
	      LOG("Couldn't get any mail preferences.\n",0);
            }
        } 

        if ( dlp_ReadAppPreference( dbi->pilot_socket, makelong("mail"), 3, 1, 
                                    0xffff, buffer, 0, 0 ) > 0 ) {
            unpack_MailSignaturePref( &sig, buffer, 0xffff );
        } 
    }
#endif

    for ( i = 0; ; i++ ) {
        struct Mail t;
        int attr;
#ifndef PILOT_LINK_0_12
        int size=0;
#endif
        recordid_t recID;
        int length;
        FILE * sendf;

#ifdef PILOT_LINK_0_12
         length = dlp_ReadNextRecInCategory( dbi->pilot_socket, dbHandle, 1,
                                            pi_buf, &recID, 0, &attr );	
#else
        length = dlp_ReadNextRecInCategory( dbi->pilot_socket, dbHandle, 1,
                                            buffer, &recID, 0, &size, &attr );	
#endif
        if ( length < 0 ) {
            break;
        }
        
        if ( ( attr & dlpRecAttrDeleted ) || ( attr & dlpRecAttrArchived ) ) {
            continue;
        }

#ifdef PILOT_LINK_0_12
	unpack_Mail( &t, pi_buf->data, pi_buf->used);
#else
        unpack_Mail( &t, buffer, length );
#endif
 
        sendf = popen( GET_CONFIG(c)->sendmail, "w" );
        if ( sendf == NULL ) {
            fprintf( stderr, "Unable to create sendmail process\n" );
            break;
        }

        if ( GET_CONFIG(c)->fromAddr ) {
            fprintf( sendf, "From: %s\n", 
                     GET_CONFIG(c)->fromAddr );
#ifdef EC_DEBUG
            fprintf( stderr, "mail: From: %s\n", 
                     GET_CONFIG(c)->fromAddr );
#endif
        }
        if ( t.to ) {
            fprintf( sendf, "To: %s\n", t.to );
#ifdef EC_DEBUG
            fprintf( stderr, "mail: To: %s\n", t.to );
#endif
        }
        if ( t.cc ) {
            fprintf( sendf, "Cc: %s\n", t.cc );
#ifdef EC_DEBUG
            fprintf( stderr, "mail: Cc: %s\n", t.cc );
#endif
        }
        if ( t.bcc ) {
            fprintf( sendf, "Bcc: %s\n", t.bcc );
#ifdef EC_DEBUG
            fprintf( stderr, "mail: Bcc: %s\n", t.bcc );
#endif
        }
        if ( t.replyTo ) {
            fprintf( sendf, "Reply-To: %s\n", t.replyTo );
#ifdef EC_DEBUG
            fprintf( stderr, "mail: Reply-To: %s\n", t.replyTo );
#endif
        }
        if ( t.subject ) {
            fprintf( sendf, "Subject: %s\n", t.subject );
#ifdef EC_DEBUG
            fprintf( stderr, "mail: Subject: %s\n", t.subject );
#endif
        }
        fprintf( sendf, "\n" );

        if ( t.body ) {
            fputs( t.body, sendf );
            fprintf( sendf, "\n" );
#ifdef EC_DEBUG
            fputs( t.body, stderr );
#endif
        }
       
        if ( t.signature && sig.signature ) {
            char *c = sig.signature;
            
            while ( ( *c == '\r' ) || ( *c == '\n' ) ) {
                c++;
            }
            if ( strncmp( c, "--", 2 ) && strncmp( c, "__", 2 ) ) {
                fprintf( sendf, "\n-- \n" );
            }
            fputs( sig.signature, sendf );
            fprintf( sendf, "\n" );
#ifdef EC_DEBUG
            fputs( sig.signature, stderr );
#endif
        }

        if ( pclose( sendf ) != 0 ) {
            free_Mail( &t );
            fprintf( stderr, "Error ending sendmail exchange\n" );
            continue;
        }

        if ( !strcmp( GET_CONFIG(c)->sendAction, "delete" ) ) {
            dlp_DeleteRecord( dbi->pilot_socket, dbHandle, 0, recID );
        } else if ( !strcmp( GET_CONFIG(c)->sendAction, 
                             "file" ) ) {
#ifdef PILOT_LINK_0_12
             dlp_WriteRecord( dbi->pilot_socket, dbHandle, attr, recID, 3, 
                             pi_buf->data, pi_buf->used, 0);
#else
            dlp_WriteRecord( dbi->pilot_socket, dbHandle, attr, recID, 3, 
                             buffer, size, 0);
#endif
        }
        free_Mail( &t );
    }
   
    /* read in all the existing records on the Palm so that we can
     * spot duplicate mails
     */
    inbox_list = (GList*) g_object_get_data(G_OBJECT(c), "inbox_list");
    if ( strcmp( GET_CONFIG(c)->receiveAction, "copy" ) == 0 ||
         strcmp( GET_CONFIG(c)->receiveAction, "mirror" ) == 0 ) {
    	for ( i = 0; ; i++ ) {
	    int attr, length;
#ifndef PILOT_LINK_0_12
	    int size;
#endif
	    recordid_t recID;
	    MailDBRecord *record;

	    /* iterate through records in category 0 (Inbox) ... */
#ifdef PILOT_LINK_0_12
 	    length = dlp_ReadNextRecInCategory( dbi->pilot_socket, dbHandle, 0,
						pi_buf, &recID, 0, &attr);
	    /* pi-dlp.h does not state that the return value is the length...
	     * so it seems safer to read the length from the pi_buf */
	    if (length >= 0)
		    length = pi_buf->used;
#else
	    length = dlp_ReadNextRecInCategory( dbi->pilot_socket, dbHandle, 0,
						buffer, &recID, 0, &size, &attr);
#endif
	    if ( length < 0 ) {
	    	break;
	    }

	    /* ... and store them in the inbox_list */
	    record = (MailDBRecord *) malloc(sizeof(*record) + length);
	    record->recID = recID;
	    record->size = length;
	    record->buffer = ((guchar *) record) + sizeof(*record);
#ifdef PILOT_LINK_0_12
	    memcpy(record->buffer, pi_buf->data, length);
#else
	    memcpy(record->buffer, buffer, length);
#endif
	    inbox_list = g_list_append(inbox_list, record);
	    LOG("storing record %d", recID);
	}
    }

    /* the above loop is likely to change the value of inbox_list so we
     * must put it back
     */
    g_object_set_data(G_OBJECT(c),"inbox_list",(gpointer)inbox_list);



    if ( GET_CONFIG(c)->mhDirectory ) {
#ifdef EC_DEBUG
        fprintf( stderr, "Reading inbound mail from %s\n",
                 GET_CONFIG(c)->mhDirectory );
#endif
        
        for( i = 1; ; i++ ) {
            int len;
            int l;
            struct Mail t;
            int mhmsg;
            
            t.to = NULL;
            t.from = NULL;
            t.cc = NULL;
            t.bcc = NULL;
            t.subject = NULL;
            t.replyTo = NULL;
            t.sentTo = NULL;
            t.body = NULL;
            t.dated = 0;
	    
            if ( ( mhmsg = openmhmsg( GET_CONFIG(c)->mhDirectory, i ) ) < 0 ) {
                break;
            }
           
#ifdef EC_DEBUG 
            fprintf( stderr, "Processing message %d", i );
#endif
            
            len = 0;
#ifdef PILOT_LINK_0_12
            while ( ( len < pi_buf->allocated ) &&
                    ( ( l = read( mhmsg, (char *)(pi_buf->data+len),
                                  pi_buf->allocated-len ) ) > 0 ) ) {
                 len += l;
             }
            pi_buf->data[len] = 0;
#else
            while ( ( len < sizeof(buffer) ) &&
                    ( ( l = read( mhmsg, (char *)(buffer+len),
                                  sizeof(buffer)-len ) ) > 0 ) ) {
                len += l;
            }
            buffer[len] = 0;
#endif
      
            if ( l < 0 ) {
                fprintf( stderr, "Error processing message %d\n", i );
                break;
            } 

#ifdef PILOT_LINK_0_12
	    if (write_message_to_pilot (c, dbi, dbHandle, pi_buf->data, i)) {
#else
	    if (write_message_to_pilot (c, dbi, dbHandle, buffer, i)) {
#endif
		rec++;
                if ( strcmp( GET_CONFIG(c)->receiveAction, "delete" ) == 0 ) {
                    char filename[1000];
                    sprintf( filename, "%s/%d", GET_CONFIG(c)->mhDirectory, 
                             i );
                    close( mhmsg );
                    if ( unlink( filename ) ) {
                        fprintf( stderr, "Error deleting message %d\n", i );
                        dupe++;
                    }
                    continue;
                } else {
                    dupe++;
                }
	    }

            close( mhmsg );
        }
    }
    
    if ( GET_CONFIG(c)->mboxFile ) {
	FILE *f;
        LOG( "Reading inbound mail from %s", GET_CONFIG(c)->mboxFile );
        f = fopen (GET_CONFIG (c)->mboxFile, "r");
	

	if (f) {
#ifdef PILOT_LINK_0_12
	    if (fgets ((char *) pi_buf->data, pi_buf->allocated - 1, f) != NULL) {
		while (!feof (f) && strncmp ((char *)pi_buf->data, "From ", 5)) {
		    if (fgets ((char *)pi_buf->data, pi_buf->allocated - 1, f) == NULL)
			break;
		}
	    }
#else
	    if (fgets (buffer, sizeof (buffer) - 1, f) != NULL) {
		while (!feof (f) && strncmp (buffer, "From ", 5)) {
		    if (fgets (buffer, sizeof (buffer) - 1, f) == NULL)
			break;
		}
	    }
#endif

	    for( i = 1; !feof (f); i++ ) {
		int len;
		char *p;
           
		LOG( "Processing message %d", i );
		len = 0;
#ifdef PILOT_LINK_0_12
		while ( ( len < pi_buf->allocated ) &&
			( ( p = fgets ( (char *)(pi_buf->data+len),
					pi_buf->allocated-len, f ) ) != 0 ) ) {		
#else
		while ( ( len < sizeof(buffer) ) &&
			( ( p = fgets ( (char *)(buffer+len),
					sizeof(buffer)-len, f ) ) != 0 ) ) {
#endif
		    if (!strncmp (p, "From ", 5)) {
			break;
		    } else {
			len += strlen (p);
		    }
		}

#ifdef PILOT_LINK_0_12
		pi_buf->data[len] = 0;
#else
		buffer[len] = 0;
#endif
		len = 0;
		
		if ( len < 0 ) {
		    fprintf( stderr, "Error processing message %d\n", i );
		    break;
		}

#ifdef PILOT_LINK_0_12
		write_message_to_pilot (c, dbi, dbHandle, pi_buf->data, i);
#else
		write_message_to_pilot (c, dbi, dbHandle, buffer, i);
#endif
	    }
	    fclose (f);
	    if ( strcmp( GET_CONFIG(c)->receiveAction, "delete" ) == 0 ) {
		if ( unlink( GET_CONFIG(c)->mboxFile ) ) {
		    fprintf( stderr, "Error deleting mbox file %s\n", 
			     GET_CONFIG(c)->mboxFile);
		}
	    }
	}   
    }

    /* in mirror mode the Palm inbox is a literal copy of the 
     * host's mbox, in this case we must remove any items
     * remaining on the inbox_list
     */
    if ( strcmp( GET_CONFIG(c)->receiveAction, "mirror" ) == 0 ) {
	GList *elem = (GList*) g_object_get_data(G_OBJECT(c), "inbox_list");
	for (; elem != NULL; elem = elem->next) {
	    MailDBRecord *record = (MailDBRecord *) elem->data;
	    LOG("purging out of date record %d", record->recID);
	    dlp_DeleteRecord( dbi->pilot_socket, dbHandle, 0, record->recID );
	}
    }

    free_MailAppInfo( &tai );
    
    dlp_ResetLastSyncPC( dbi->pilot_socket );
    dlp_CloseDB( dbi->pilot_socket, dbHandle );

#ifdef PILOT_LINK_0_12
    pi_buffer_free (pi_buf);
#endif

    return( 0 );
}

gint copy_from_pilot( GnomePilotConduit *c, GnomePilotDBInfo *dbi ) {
    return( synchronize( c, dbi ) );
}

static GtkWidget
*createCfgWindow (GnomePilotConduit* conduit)
{
    GtkWidget *vbox, *table;
    GtkWidget *label, *widget;

    vbox = gtk_vbox_new(FALSE, EC_PAD);

    table = gtk_table_new(2, 5, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 4);
    gtk_table_set_col_spacings(GTK_TABLE(table), 10);
    gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, EC_PAD);

    /* send_action option menu */
    label = gtk_label_new(_("Send Action:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    widget = gtk_combo_box_text_new ();
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget),  (_("Delete from PDA")));
    g_object_set_data(G_OBJECT(widget), EC_SEND_ACTION[0], GINT_TO_POINTER(0));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget), _("File on PDA"));
    g_object_set_data(G_OBJECT(widget), EC_SEND_ACTION[1], GINT_TO_POINTER(1));
    gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);
    gtk_table_attach_defaults(GTK_TABLE(table), widget, 1, 2, 0, 1);
    g_object_set_data(G_OBJECT(vbox), "send_action", widget);

    /* from_address entry */
    label = gtk_label_new(_("From:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    widget = gtk_entry_new();
    gtk_entry_set_max_length(GTK_ENTRY(widget), 128);
    gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);
    gtk_table_attach_defaults(GTK_TABLE(table), widget, 1, 2, 1, 2);
    g_object_set_data(G_OBJECT(vbox), "from_address", widget);

    /* sendmail entry */
    label = gtk_label_new(_("Sendmail command:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    widget = gtk_entry_new();
    gtk_entry_set_max_length(GTK_ENTRY(widget), 128);

    gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 2, 3);
    gtk_table_attach_defaults(GTK_TABLE(table), widget, 1, 2, 2, 3);
    g_object_set_data(G_OBJECT(vbox), "sendmail", widget);

    /* receive_action option menu */
    label = gtk_label_new (_("Receive Action:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    widget = gtk_combo_box_text_new ();
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget), _("Copy from Inbox"));
    g_object_set_data(G_OBJECT(widget), EC_RECV_ACTION[0], GINT_TO_POINTER(0));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget), _("Delete from Inbox"));
    g_object_set_data(G_OBJECT(widget), EC_RECV_ACTION[1], GINT_TO_POINTER(1));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(widget), _("Mirror Inbox"));
    g_object_set_data(G_OBJECT(widget), EC_RECV_ACTION[2], GINT_TO_POINTER(2));
    gtk_table_attach_defaults (GTK_TABLE(table), label, 0, 1, 3, 4);
    gtk_table_attach_defaults(GTK_TABLE(table), widget, 1, 2, 3, 4);
    g_object_set_data(G_OBJECT(vbox), "receive_action", widget);


    /* mbox_file entry */
    label = gtk_label_new(_("Copy mail from:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

    widget = gtk_file_chooser_button_new (_("Select a file"),
	GTK_FILE_CHOOSER_ACTION_OPEN);

    gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 4, 5);
    gtk_table_attach_defaults(GTK_TABLE(table), widget, 1, 2, 4, 5);
    g_object_set_data(G_OBJECT(vbox), "mbox_file", widget);

    return vbox;
}

static void
setOptionsCfg(GtkWidget *cfg, ConduitCfg *c)
{
    GtkWidget *send_action, *from_address, *sendmail, *receive_action, *mbox_file;
    guint id;

    /* fetch all the controls from the cfg window */
    send_action = g_object_get_data(G_OBJECT(cfg), "send_action");
    from_address = g_object_get_data(G_OBJECT(cfg), "from_address");
    sendmail = g_object_get_data(G_OBJECT(cfg), "sendmail");
    receive_action = g_object_get_data(G_OBJECT(cfg), "receive_action");
    mbox_file = g_object_get_data(G_OBJECT(cfg), "mbox_file");

    id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(send_action), c->sendAction));
    gtk_combo_box_set_active(GTK_COMBO_BOX(send_action), id);

    gtk_entry_set_text(GTK_ENTRY(from_address), (c->fromAddr ? c->fromAddr : ""));
    gtk_entry_set_text(GTK_ENTRY(sendmail), (c->sendmail ? c->sendmail : ""));

    id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(receive_action), c->receiveAction));
    gtk_combo_box_set_active(GTK_COMBO_BOX(receive_action), id);
     
    if (c->mboxFile && 0 != strcmp(c->mboxFile, "")) {
        gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(mbox_file), c->mboxFile);
    } else if (c->mhDirectory) {
    	gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(mbox_file), c->mhDirectory);
    } else {
    	gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(mbox_file), "");
    }
}

static void
readOptionsCfg(GtkWidget *cfg, ConduitCfg *c)
{
    GtkWidget *send_action, *from_address, *sendmail, *receive_action, *mbox_file;
    gchar *str;
    gint id;
    struct stat mboxStat;

    /* fetch all the controls from the cfg window */
    send_action = g_object_get_data(G_OBJECT(cfg), "send_action");
    from_address = g_object_get_data(G_OBJECT(cfg), "from_address");
    sendmail = g_object_get_data(G_OBJECT(cfg), "sendmail");
    receive_action = g_object_get_data(G_OBJECT(cfg), "receive_action");
    mbox_file = g_object_get_data(G_OBJECT(cfg), "mbox_file");

    id = gtk_combo_box_get_active(GTK_COMBO_BOX(send_action));
    str = g_strdup(EC_SEND_ACTION[id]);
    g_free(c->sendAction);
    c->sendAction = str;

    str = gtk_editable_get_chars(GTK_EDITABLE(from_address), 0, -1);
    if (0 == strcmp(str, "")) {
        g_free(str);
	str = NULL;
    }
    g_free(c->fromAddr);
    c->fromAddr = str;

    str = gtk_editable_get_chars(GTK_EDITABLE(sendmail), 0, -1);
    if (0 == strcmp(str, "")) {
	g_free(str);
	str = NULL;
    }
    g_free(c->sendmail);
    c->sendmail = str;
     
    id = gtk_combo_box_get_active(GTK_COMBO_BOX(receive_action));
    str = g_strdup(EC_RECV_ACTION[id]);
    g_free(c->receiveAction);
    c->receiveAction = str;
     
    str = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(mbox_file));
    if (0 == strcmp(str, "")) {
        g_free(str);
	str = NULL;
    }
    g_free(c->mboxFile);
    c->mboxFile = NULL;
    g_free(c->mhDirectory);
    c->mhDirectory = NULL;
    if (str) {
	 if (0 == stat(str, &mboxStat) && S_ISDIR(mboxStat.st_mode)) {
	     c->mhDirectory = str;
	 } else {
             c->mboxFile = str;
	 }
     }
}

static gint
create_settings_window (GnomePilotConduit *conduit, GtkWidget *parent, gpointer data)
{
    GtkWidget *cfgWindow;
    cfgWindow = createCfgWindow(conduit);
    gtk_container_add(GTK_CONTAINER(parent),cfgWindow);
    gtk_widget_show_all(cfgWindow);
    
    g_object_set_data(G_OBJECT(conduit),OBJ_DATA_CONFIG_WINDOW,cfgWindow);
    setOptionsCfg(GET_CONDUIT_WINDOW(conduit),GET_CONFIG(conduit));

    return 0;
}

static void
display_settings (GnomePilotConduit *conduit, gpointer data)
{
    setOptionsCfg(GET_CONDUIT_WINDOW(conduit),GET_CONFIG(conduit));
}

static void
save_settings    (GnomePilotConduit *conduit, gpointer data)
{
    readOptionsCfg(GET_CONDUIT_WINDOW(conduit),GET_CONFIG(conduit));
    save_configuration(GET_CONFIG(conduit));
}

static void
revert_settings  (GnomePilotConduit *conduit, gpointer data)
{
    ConduitCfg *cfg,*cfg2;

    cfg2= GET_OLDCONFIG(conduit);
    cfg = GET_CONFIG(conduit);
    save_configuration(cfg2);
    copy_configuration(cfg,cfg2);
    setOptionsCfg(GET_CONDUIT_WINDOW(conduit),cfg);
}

GnomePilotConduit *conduit_get_gpilot_conduit( guint32 pilotId ) 
{
  GObject *retval;
  ConduitCfg *cfg1, *cfg2;

  retval = gnome_pilot_conduit_standard_new("MailDB",0x6d61696c, NULL);

  g_assert(retval != NULL);

  /* conduit signals */
  /*
  g_signal_connect(retval, "copy_from_pilot", (GCallback)copy_from_pilot ,NULL);
  g_signal_connect(retval, "copy_to_pilot", (GCallback) ,NULL);
  g_signal_connect(retval, "merge_to_pilot", (GCallback) ,NULL);
  g_signal_connect(retval, "merge_from_pilot", (GCallback)synchronize ,NULL);
  */
  g_signal_connect(retval, "synchronize", (GCallback)synchronize ,NULL);

  /* GUI signals */
  g_signal_connect(retval, "create_settings_window", (GCallback)create_settings_window ,NULL);
  g_signal_connect(retval, "display_settings", (GCallback)display_settings ,NULL);
  g_signal_connect(retval, "save_settings", (GCallback)save_settings ,NULL);
  g_signal_connect(retval, "revert_settings", (GCallback)revert_settings ,NULL);

  load_configuration(&cfg1, pilotId );
  cfg2 = dupe_configuration(cfg1);
  g_object_set_data(G_OBJECT(retval),OBJ_DATA_CONFIG,(gpointer)cfg1);
  g_object_set_data(G_OBJECT(retval),OBJ_DATA_OLDCONFIG,(gpointer)cfg2);

  return GNOME_PILOT_CONDUIT(retval); 
}

void conduit_destroy_gpilot_conduit( GnomePilotConduit *c ) 
{
  ConduitCfg *cfg1, *cfg2;
  GList *inbox_list, *list;
  
  cfg1 = GET_CONFIG(c);
  cfg2 = GET_OLDCONFIG(c);
  destroy_configuration( &cfg1 );
  destroy_configuration( &cfg2 );

  inbox_list = (GList*) g_object_get_data(G_OBJECT(c), "inbox_list");
  for (list = inbox_list; list != NULL; list = list->next) {
    free(list->data);
  }
  g_list_free(inbox_list);
  inbox_list = NULL;

  g_object_unref (G_OBJECT (c));
}

#define OLD_PREFIX ".gnome2/gnome-pilot.d"
#define NEW_PREFIX ".gnome-pilot"

#define IS_STR_SET(x) (x != NULL && x[0] != '\0')

static void
migrate_conf (const gchar *old, const gchar *new)
{
	gchar *basename = g_path_get_dirname (new);

	if (!g_file_test (basename, G_FILE_TEST_EXISTS)) {
		g_mkdir_with_parents (basename, S_IRUSR | S_IWUSR | S_IXUSR);
	} else {
		if (!g_file_test (basename, G_FILE_TEST_IS_DIR)) {
			gchar *tmp = g_strdup_printf ("%s.old", basename);
			rename (basename, tmp);
			g_free (tmp);
			g_mkdir_with_parents (basename, S_IRUSR | S_IWUSR | S_IXUSR);
		}
	}
	g_free (basename);

	if (g_file_test (new, G_FILE_TEST_IS_REGULAR)) {
		return;
	} else if (g_file_test (old, G_FILE_TEST_IS_REGULAR)) {
		rename (old, new);
	} else {
		creat (new, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	}
}

static GKeyFile*
get_kfile (const gchar *conf)
{
	GKeyFile   *kfile = g_key_file_new ();
	const char *homedir = g_getenv ("HOME");
	char       *old = NULL;
	char       *new = NULL;

	if (!homedir)
		homedir = g_get_home_dir ();

	old = g_build_filename (homedir, OLD_PREFIX, conf, NULL);
	new = g_build_filename (homedir, NEW_PREFIX, conf, NULL);

	migrate_conf (old, new);

	g_key_file_load_from_file (kfile, new, G_KEY_FILE_NONE, NULL);
	g_key_file_set_list_separator (kfile, ' ');

	g_free (new);
	g_free (old);
	return kfile;
}

static gboolean
save_config (GKeyFile    *kfile,
	     const gchar *conf)
{
	const char *homedir = g_getenv ("HOME");
        GError     *error = NULL;
        gchar      *data = NULL;
        gsize       size;
	gchar 	   *filename = NULL;

	g_return_val_if_fail (kfile, FALSE);
	g_return_val_if_fail (IS_STR_SET (conf), FALSE);

	if (!homedir)
		homedir = g_get_home_dir ();

	filename = g_build_filename (homedir, NEW_PREFIX, conf, NULL);

	if (! g_file_test (filename, G_FILE_TEST_IS_REGULAR)) {
		g_free (filename);
		g_warning ("File %s does not exsit", filename);
		return FALSE;
	}

        g_message ("Saving config to disk...");

	g_key_file_set_list_separator (kfile, ' ');
 	data = g_key_file_to_data (kfile, &size, &error);
        if (error) {
                g_warning ("Could not get config data to write to file, %s",
                           error->message);
                g_error_free (error);

                return FALSE;
        }

        g_file_set_contents (filename, data, size, &error);
        g_free (data);

        if (error) {
                g_warning ("Could not write %" G_GSIZE_FORMAT " bytes to file '%s', %s",
                           size,
                           filename,
                           error->message);
                g_free (filename);
                g_error_free (error);

                return FALSE;
        }

        g_message ("Wrote config to '%s' (%" G_GSIZE_FORMAT " bytes)",
                   filename,
                   size);

	g_free (filename);

	return TRUE;
}
