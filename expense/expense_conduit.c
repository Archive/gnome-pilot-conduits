/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

/* expense conduit, based on read-expense */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <pi-source.h>
#include <pi-socket.h>
#include <pi-dlp.h>
#include <pi-version.h>

#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <utime.h>
#include <unistd.h>
#include <pwd.h>
#include <signal.h>
#include <time.h>

#include <gnome-pilot-conduit.h>
#include <gnome-pilot-conduit-standard.h>
#include "expense_conduit.h"

#define CONDUIT_VERSION "0.3"

#define EC_PAD 8

GnomePilotConduit *conduit_get_gpilot_conduit( guint32 pilotId );
void conduit_destroy_gpilot_conduit( GnomePilotConduit *c );
static gboolean save_config (GKeyFile    *kfile, const gchar *conf);
static GKeyFile* get_kfile (const gchar *conf);
static void migrate_conf (const gchar *old, const gchar *new);

/* Following depend on the ordering in pi-expense.h ! */
static gchar *ExpenseTypeName[] = { "Airfare", "Breakfast", "Bus", "BusinessMeals", "CarRental", 
                                    "Dinner", "Entertainment", "Fax", "Gas", "Gifts", "Hotel", 
                                    "Incidentals","Laundry", "Limo", "Lodging", "Lunch", "Mileage", 
                                    "Other", "Parking", "Postage", "Snack", "Subway", "Supplies", 
                                    "Taxi", "Telephone", "Tips", "Tolls", "Train" };

static gchar *ExpensePaymentName[] = { "AmEx", "Cash", "Check", "CreditCard", "MasterCard", 
                                       "PrePaid", "VISA", "Unfiled" };

/* these values are hardcoded in the palm expense appl. does it differ for non-us palms??? */
static gchar *ExpenseCurrencyName[] = { "AU$", "S", "BF", "R$", "$CN", "DKK", "Mk", "FRF", "DM", 
                                        "HK$", "ISK", "IEP", "L.", "Y", "Flux", "MXP", "NLG", 
                                        "$NZ", "NOK", "Pts", "SEK", "CHF", "GBP", "$", "EU" };

/* #define EC_DEBUG */
#ifdef EC_DEBUG
#define LOG(format,args...) g_log (G_LOG_DOMAIN, \
                                   G_LOG_LEVEL_MESSAGE, \
                                   "expense: "##format, ##args)
#else
#define LOG(format,args...)
#endif

static void 
load_configuration(ConduitCfg **c,guint32 pilotId) 
{
	gchar *iPilot;
	GKeyFile *kfile;
	GError *error = NULL;

	gchar *tempbuf;

	g_assert(c!=NULL);
	*c = g_new0(ConduitCfg,1);
	(*c)->child = -1;

 	kfile = get_kfile("expense-conduit");
	iPilot = g_strdup_printf ("Pilot_%u", pilotId);

  	(*c)->dir = g_key_file_get_string (kfile, iPilot, "dir", &error);
        if (error) {
                (*c)->dir = NULL;
                error = NULL;
        }

	(*c)->dateFormat = g_key_file_get_string (kfile, iPilot, "date_format", &error);
        if (error) {
                (*c)->dateFormat = g_strdup("%x");
                error = NULL;
        }

	(*c)->outputFormat = g_key_file_get_integer (kfile, iPilot, "output_format", &error);
        if (error) {
                (*c)->outputFormat = 0;
                error = NULL;
        }

        tempbuf = g_key_file_get_string (kfile, iPilot, "dir mode", &error);
        if (error) {
                tempbuf = g_strdup("0700");
                error = NULL;
        }
	(*c)->dirMode =(mode_t)strtol(tempbuf,NULL,0);
	g_free(tempbuf);

        tempbuf = g_key_file_get_string (kfile, iPilot, "file mode", &error);
        if (error) {
                tempbuf = g_strdup("0600");
                error = NULL;
        }
        (*c)->fileMode =(mode_t)strtol(tempbuf,NULL,0);
	g_free(tempbuf);


	(*c)->pilotId = pilotId;
	g_free (iPilot);
        g_key_file_free (kfile);
}

static void 
save_configuration(ConduitCfg *c) 
{
	gchar *iPilot;
	GKeyFile *kfile;

        char buf[20];

	g_assert(c!=NULL);

 	kfile = get_kfile("expense-conduit");
	iPilot = g_strdup_printf ("Pilot_%u", c->pilotId);

        g_key_file_set_string (kfile, iPilot, "dir", c->dir);
        g_key_file_set_string (kfile, iPilot, "date_format", c->dateFormat);
        g_key_file_set_integer (kfile, iPilot, "output_format", c->outputFormat);
	g_snprintf(buf,sizeof(buf),"0%o", c->dirMode);
        g_key_file_set_string (kfile, iPilot, "dir mode", buf);
	g_snprintf(buf,sizeof(buf),"0%o", c->fileMode);
        g_key_file_set_string (kfile, iPilot, "file mode", buf);

	g_free (iPilot);

	save_config (kfile, "expense-conduit");
}
 
static void 
copy_configuration(ConduitCfg *d, ConduitCfg *c)
{
        g_return_if_fail(c!=NULL);
        g_return_if_fail(d!=NULL);

	d->dir = g_strdup(c->dir);
	d->dateFormat = g_strdup(c->dateFormat);
	d->outputFormat = c->outputFormat;
	d->dirMode = c->dirMode;
	d->fileMode = c->fileMode;

	d->pilotId = c->pilotId;
}

static ConduitCfg*
dupe_configuration(ConduitCfg *c) 
{
	ConduitCfg *retval;

	g_assert(c!=NULL);

	retval = g_new0(ConduitCfg,1);
        copy_configuration(retval,c);
        
	return retval;
}

/** this method frees all data from the conduit config */
static void 
destroy_configuration(ConduitCfg **c) 
{
	g_assert(c!=NULL);
	g_assert(*c!=NULL);
	g_free( (*c)->dir);
	g_free( (*c)->dateFormat);
	g_free(*c);
	*c = NULL;
}


/* from pilot-xfer */
static void protect_name(char *d, char *s) 
{
	while(*s) {
		switch(*s) {
		case '/': *(d++) = '='; *(d++) = '2'; *(d++) = 'F'; break;
		case '=': *(d++) = '='; *(d++) = '3'; *(d++) = 'D'; break;
		case '\x0A': *(d++) = '='; *(d++) = '0'; *(d++) = 'A'; break;
		case '\x0D': *(d++) = '='; *(d++) = '0'; *(d++) = 'D'; break;
			/*case ' ': *(d++) = '='; *(d++) = '2'; *(d++) = '0'; break;*/
		default: 
			if(*s < ' ') {
				gchar tmp[6];
				g_snprintf(tmp,5,"=%2X",(unsigned char)*s);
				*(d++) = tmp[0]; *(d++) = tmp[1]; *(d++) = tmp[2];
			} else
				*(d++) = *s;
			break;
		}
		++s;
	}
	*d = '\0';
}

/** 
    generates a pathname for a category 
 */
static gchar *category_path(int category, GnomePilotConduit *c) 
{
	static gchar filename[FILENAME_MAX];
	gchar buf[64];
	
	if(category < 0 || category >= 16)
		strcpy(buf,"BogusCategory");
	else
		protect_name(buf,GET_CONDUIT_DATA(c)->ai.category.name[category]);
  
	g_snprintf(filename,FILENAME_MAX-1,"%s/%s",
		   GET_CONDUIT_CFG(c)->dir,
		   buf);
	     
	return filename;
}

static void writeout_record(int fd, struct Expense *record, GnomePilotConduit *c)
{
        char entry[0xffff];
    
        const int kDateStrSize = 30;
        char DateStr[kDateStrSize];
        char *Currency;

        strftime(DateStr, kDateStrSize, GET_CONDUIT_CFG(c)->dateFormat, &record->date);

        if(record->currency < 24)
                Currency = ExpenseCurrencyName[record->currency];
        /* handle the euro*/ 
        else if(record->currency == 133)
                Currency = ExpenseCurrencyName[24];
        /* handle the custom currencies */
        else if(record->currency >= 128 && record->currency < 133) 
                Currency = GET_CONDUIT_DATA(c)->ai.currencies[record->currency-128].symbol;
        else {
                g_warning(_("Unknown Currency Symbol"));
                Currency = "";
        }

        switch(GET_CONDUIT_CFG(c)->outputFormat) {
        case eSimpleFormat:
                sprintf(entry, "%s, %s, %s, %s, %s\n", DateStr, ExpenseTypeName[record->type], ExpensePaymentName[record->payment], Currency, record->amount ? record->amount: "<None>");
                break;
        case eComplexFormat:
        default:
                sprintf(entry, "Date: %s, Type: %s, Payment: %s, Currency: %s, Amount: '%s', Vendor: '%s', City: '%s', Attendees: '%s', Note: '%s'\n", DateStr, ExpenseTypeName[record->type], ExpensePaymentName[record->payment], Currency, record->amount ? record->amount: "<None>", record->vendor ? record->vendor: "<None>", record->city ? record->city: "<None>", record->attendees ? record->attendees: "<None>", record->note ? record->note: "<None>");
        }

#ifdef EC_DEBUG
        fprintf(stderr, "%s\n", entry);
#endif
        if(write(fd, entry, strlen(entry)) == -1) {
                perror("writeout_record");
        }
}

/* Parts of this routine is shamelessly stolen from read-expenses.c from the pilot-link 
package, Copyright (c) 1997, Kenneth Albanowski
*/
static gint copy_from_pilot( GnomePilotConduit *c, GnomePilotDBInfo *dbi )
{
        int dbHandle;
#ifdef PILOT_LINK_0_12
        pi_buffer_t *pi_buf;
#else
        guchar buffer[0xffff];
#endif

        struct ExpenseAppInfo *tai;
        struct ExpensePref *tp;

        int i;
        int ret;
        const int numhandles = 16; /* number of categories. See pi-appinfo.h  */
        int filehandle[numhandles];
        int result = 0;

        for (i = 0; i < numhandles; i++)
                filehandle[i] = -1;

        if(GET_CONDUIT_CFG(c)->dir==NULL || strlen(GET_CONDUIT_CFG(c)->dir)==0) {
		g_warning(_("No dir specified. Please run expense conduit capplet first."));
		gnome_pilot_conduit_send_error(c,
                                               _("No dir specified. Please run expense conduit capplet first."));
        }

        tai = &(GET_CONDUIT_DATA(c)->ai);
        tp = &(GET_CONDUIT_DATA(c)->pref);

        g_message ("Expense Conduit v.%s", CONDUIT_VERSION);

        if(dlp_OpenDB(dbi->pilot_socket, 0, 0x80|0x40, "ExpenseDB", &dbHandle) < 0) {
                g_warning("Unable to open ExpenseDB");
                return -1;
        }
    
#ifdef PILOT_LINK_0_12
        pi_buf = pi_buffer_new (0xffff);     
        unpack_ExpensePref(tp, pi_buf->data, 0xffff);        
#else
        unpack_ExpensePref(tp, buffer, 0xffff);
#endif

#ifdef EC_DEBUG
        fprintf(stderr, "Orig prefs, %d bytes:\n", ret);
        fprintf(stderr, "Expense prefs, current category %d, default category %d\n",
                tp->currentCategory, tp->defaultCategory);
        fprintf(stderr, "  Note font %d, Show all categories %d, Show currency %d, Save backup %d\n",
                tp->noteFont, tp->showAllCategories, tp->showCurrency, tp->saveBackup);
        fprintf(stderr, "  Allow quickfill %d, Distance unit %d, Currencies:\n",
                tp->allowQuickFill, tp->unitOfDistance);
        for(i = 0; i < 7; i++) {
                fprintf(stderr, " %d", tp->currencies[i]);
        }
        fprintf(stderr, "\n");
#endif /* EC_DEBUG */

#ifdef PILOT_LINK_0_12
        ret = dlp_ReadAppBlock(dbi->pilot_socket, dbHandle, 0, 0xffff, pi_buf);
        unpack_ExpenseAppInfo(tai, pi_buf->data, 0xffff);
#else
        ret = dlp_ReadAppBlock(dbi->pilot_socket, dbHandle, 0, buffer, 0xffff);
        unpack_ExpenseAppInfo(tai, buffer, 0xffff);
#endif

#ifdef EC_DEBUG
        fprintf(stderr, "Orig length %d, new length %d, orig data:\n", ret, i);
        fprintf(stderr, "New data:\n");
    
        fprintf(stderr, "Expense app info, sort order %d\n", tai->sortOrder);
        for(i = 0; i < 4; i++)
                fprintf(stderr, " Currency %d, name '%s', symbol '%s', rate '%s'\n", i, 
                        tai->currencies[i].name, tai->currencies[i].symbol, tai->currencies[i].rate);
#endif /* EC_DEBUG */

        /* make the directory */
        if(mkdir(GET_CONDUIT_CFG(c)->dir, GET_CONDUIT_CFG(c)->dirMode) < 0) {
                if(errno != EEXIST) {
                        g_warning ("Unable to create directory:\n\t%s\n\t%s\n", 
                                   GET_CONDUIT_CFG(c)->dir,
                                   strerror (errno));
                        goto error;
                }
        }
        
        /* open one file for every category in Expense */
        for(i = 0; i < numhandles; i++) {
                /* skip unused categories */
                if(*tai->category.name[i] != '\0') {
                        LOG("Opening for cat %d: %s\n", i, category_path(i, c));
                        if((filehandle[i] = creat(category_path(i, c), GET_CONDUIT_CFG(c)->fileMode))== -1) {
                                LOG("copy_from_pilot: error in opening %s", category_path(i, c));
                                perror("");
                                goto error;
                        }
                } else {
                        filehandle[i] = -1;
                }
        }

        /* loop through all the records */
        for (i = 0; ; i++) {
                struct Expense t;
                int attr, category, len;

#ifdef PILOT_LINK_0_12
                len = dlp_ReadRecordByIndex(dbi->pilot_socket, dbHandle, i, pi_buf, 0, &attr, &category);
#else
                len = dlp_ReadRecordByIndex(dbi->pilot_socket, dbHandle, i, buffer, 0, 0, &attr, &category);
#endif
                
                /* at the end of all the records? */
                if(len < 0)
                        break;
                /* Skip deleted records */
                if((attr & dlpRecAttrDeleted) || (attr & dlpRecAttrArchived))
                        continue;

#ifdef PILOT_LINK_0_12
                unpack_Expense(&t, pi_buf->data, len);
#else
                unpack_Expense(&t, buffer, len);                
#endif
                /* PalmOS should give us a 4-bit category, and that's all that
                 * pi-appinfo.h can cope with, but lets be cautious */
                if (category < 0 || category >= 16) {
                        g_warning ("Out-of-range category ID from device: %d\n", category);
                        goto error;
                }
                if (filehandle[category] == -1) {
                        g_warning ("Unexpected category ID from device: %d\n", category);
                        goto error;
                }
                writeout_record(filehandle[category], &t, c);
                free_Expense(&t);
        }

        goto exit;
 error:
        result = -1;
 exit:
        /* close all the opened filehandles */
        for(i = 0; i < numhandles; i++)
                if(filehandle[i] != -1)
                        close(filehandle[i]);

        /* Close the database */
        dlp_CloseDB(dbi->pilot_socket, dbHandle);
        
#ifdef PILOT_LINK_0_12
        if (pi_buf) {
                pi_buffer_free (pi_buf);
        }
#endif
         
        return( result );
}

static gint synchronize( GnomePilotConduit *c, GnomePilotDBInfo *dbi ) {
        return copy_from_pilot( c, dbi );
}

/*
 * Gui Configuration Code
 */
static void
insert_ignore_space_cb (GtkEditable    *editable, const gchar    *text,
                        gint len, gint *position, void *data)
{
        gint i;
        const gchar *curname;

        curname = gtk_entry_get_text(GTK_ENTRY(editable));
        if (*curname == '\0' && len > 0) {
                if (isspace(text[0])) {
                        g_signal_stop_emission_by_name(G_OBJECT(editable), "insert_text");
                        return;
                }
        } else {
                for (i=0; i<len; i++) {
                        if (isspace(text[i])) {
                                g_signal_stop_emission_by_name(G_OBJECT(editable), 
                                                             "insert_text");
                                return;
                        }
                }
        }
}

static void
insert_numeric_cb(GtkEditable    *editable, const gchar    *text,
                  gint len, gint *position, void *data)
{
	gint i;

	for (i=0; i<len; i++) {
		if (!isdigit(text[i])) {
			g_signal_stop_emission_by_name(G_OBJECT(editable), "insert_text");
			return;
		}
	}
}


#define DATE_OPTIONS_COUNT 4
typedef struct {
        gchar *name;
        char *format;
} DateSetting_t;

static DateSetting_t date_options[] = { { N_("Day/Month/Year"), "%d/%m/%Y"}, 
                                       { N_("Month/Day/Year"), "%m/%d/%Y"}, 
                                       { N_("Since 1970-01-01 (in sec)"), "%s"}, 
                                       { N_("Local format"), "%x"}
};

#define WRITEOUT_OPTIONS_COUNT 2
typedef struct {
        gchar *name;
        enum ExpenseOutputFormat format;
} WriteoutSetting_t;

static WriteoutSetting_t writeout_options[] = { { N_("Simple"), eSimpleFormat},
                                                { N_("Complex"), eComplexFormat} };

typedef struct _FieldInfo FieldInfo;
struct _FieldInfo
{
	gchar    *name;
	gchar    *label_data;
	gchar    *obj_data;
	gpointer  insert_func;
};


static FieldInfo fields[] = { { N_("Expense Directory:"), NULL, "ExpenseDir", insert_ignore_space_cb},
                       { N_("Directory Mode:"), NULL, "DirMode", insert_numeric_cb},
                       { N_("File Mode:"), NULL, "FileMode", insert_numeric_cb}, 
                       { NULL, NULL, NULL, NULL}
};

static GtkWidget
*createCfgWindow(void)
{
	GtkWidget *vbox, *table;
	GtkWidget *entry, *label;
        GtkWidget *optionMenu;

        int i, count=0, widget_offset;

	vbox = gtk_vbox_new(FALSE, EC_PAD);

	table = gtk_table_new(2, 5, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE(table), 4);
	gtk_table_set_col_spacings(GTK_TABLE(table), 10);
	gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, EC_PAD);

        /* set the date format */
        label = gtk_label_new(_("Date Format:"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);

        optionMenu = gtk_combo_box_text_new();
        for (i = 0; i < DATE_OPTIONS_COUNT; i++) {
                gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(optionMenu),
                    _(date_options[i].name));
        }

        gtk_table_attach_defaults(GTK_TABLE(table), optionMenu, 1, 2, 1, 2);
        g_object_set_data(G_OBJECT(vbox), "DateFormat", optionMenu);

        /* set the writeout format */
        label = gtk_label_new(_("Output Format:"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);

        gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 2, 3);

        optionMenu = gtk_combo_box_text_new(); 
        for (i = 0; i < WRITEOUT_OPTIONS_COUNT; i++) {
                gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT (optionMenu),
                    _(writeout_options[i].name));
        }

        gtk_table_attach_defaults(GTK_TABLE(table), optionMenu, 1, 2, 2, 3);
        g_object_set_data(G_OBJECT(vbox), "OutputFormat", optionMenu);

        /* ugh, so we have an asymmetry here: above is done in paste&copy fashion 
           and below, we do it nicely with structs and stuff */  

        /* do the dir & modes */

	/* how many fields do we have */
	while(fields[count].name!=0) count++;

        widget_offset = 3;
	for(i = 0; i < count; i++) {
		label = gtk_label_new(_(fields[i].name));
                gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
                gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1,
                	widget_offset+i, widget_offset+i+1);
		if(fields[i].label_data!=NULL) {
			g_object_set_data(G_OBJECT(vbox), fields[i].label_data, label);
		}
		entry = gtk_entry_new();
                gtk_entry_set_max_length(GTK_ENTRY(entry), 128);
		g_object_set_data(G_OBJECT(vbox), fields[i].obj_data, entry);
		gtk_table_attach(GTK_TABLE(table), entry, 1, 2, 
                                 widget_offset+i, widget_offset+i+1, 0,0,0,0);
		g_signal_connect(G_OBJECT(entry), "insert_text",
				   G_CALLBACK(fields[i].insert_func),
				   NULL);
	}
	

	return vbox;
}

static void
setOptionsCfg(GtkWidget *pilotcfg, ConduitCfg *state)
{
	GtkWidget *DateFormat, *OutputFormat, *ExpenseDir, *DirMode, *FileMode;
        gchar buf[8];

        int i;

	DateFormat = g_object_get_data(G_OBJECT(pilotcfg), "DateFormat");
	OutputFormat = g_object_get_data(G_OBJECT(pilotcfg), "OutputFormat");
	ExpenseDir = g_object_get_data(G_OBJECT(pilotcfg), "ExpenseDir");
	DirMode = g_object_get_data(G_OBJECT(pilotcfg), "DirMode");
	FileMode = g_object_get_data(G_OBJECT(pilotcfg), "FileMode");

	g_assert(DateFormat != NULL);
	g_assert(OutputFormat != NULL);
	g_assert(ExpenseDir != NULL);
	g_assert(DirMode != NULL);
	g_assert(FileMode != NULL);
                
	gtk_entry_set_text(GTK_ENTRY(ExpenseDir), state->dir);
	g_snprintf(buf, 7, "0%o", state->dirMode);
	gtk_entry_set_text(GTK_ENTRY(DirMode),buf);
	g_snprintf(buf, 7, "0%o", state->fileMode);
	gtk_entry_set_text(GTK_ENTRY(FileMode),buf);

        /* find the entry in the option menu. if not found, default to the last */
        for(i = 0; i < DATE_OPTIONS_COUNT && g_ascii_strncasecmp(state->dateFormat, date_options[i].format, 20) != 0; i++);
        gtk_combo_box_set_active(GTK_COMBO_BOX(DateFormat), i);

        for(i = 0; i < WRITEOUT_OPTIONS_COUNT && state->outputFormat != writeout_options[i].format; i++);
        gtk_combo_box_set_active(GTK_COMBO_BOX(OutputFormat), i);
}

static void
readOptionsCfg(GtkWidget *pilotcfg, ConduitCfg *state)
{
	GtkWidget *ExpenseDir, *DirMode, *FileMode;
        GtkWidget *combo_box;
        
	ExpenseDir = g_object_get_data(G_OBJECT(pilotcfg), "ExpenseDir");
	DirMode = g_object_get_data(G_OBJECT(pilotcfg), "DirMode");
	FileMode = g_object_get_data(G_OBJECT(pilotcfg), "FileMode");

        state->dir = g_strdup(gtk_entry_get_text(GTK_ENTRY(ExpenseDir)));
        state->dirMode = strtol(gtk_entry_get_text(GTK_ENTRY(DirMode)), NULL, 0);
        state->fileMode = strtol(gtk_entry_get_text(GTK_ENTRY(FileMode)), NULL, 0);

        combo_box = g_object_get_data(G_OBJECT(pilotcfg), "DateFormat");
        state->dateFormat = g_strdup(date_options[
                gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box))].format);

        combo_box = g_object_get_data(G_OBJECT(pilotcfg), "OutputFormat");
        state->outputFormat = writeout_options[
            gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box))].format;
        
}

static gint
create_settings_window (GnomePilotConduit *conduit, GtkWidget *parent, gpointer data)
{
	GtkWidget *cfgWindow;

	cfgWindow = createCfgWindow();
	gtk_container_add(GTK_CONTAINER(parent),cfgWindow);
	gtk_widget_show_all(cfgWindow);

	g_object_set_data(G_OBJECT(conduit),OBJ_DATA_CONFIG_WINDOW,cfgWindow);
	setOptionsCfg(GET_CONDUIT_WINDOW(conduit),GET_CONDUIT_CFG(conduit));

	return 0;
}

static void
display_settings (GnomePilotConduit *conduit, gpointer data)
{
	setOptionsCfg(GET_CONDUIT_WINDOW(conduit),GET_CONDUIT_CFG(conduit));
}

static void
save_settings    (GnomePilotConduit *conduit, gpointer data)
{
	readOptionsCfg(GET_CONDUIT_WINDOW(conduit),GET_CONDUIT_CFG(conduit));
	save_configuration(GET_CONDUIT_CFG(conduit));
}

static void
revert_settings  (GnomePilotConduit *conduit, gpointer data)
{
	ConduitCfg *cfg,*cfg2;

	cfg2= GET_CONDUIT_OLDCFG(conduit);
	cfg = GET_CONDUIT_CFG(conduit);
	save_configuration(cfg2);
	copy_configuration(cfg,cfg2);
	setOptionsCfg(GET_CONDUIT_WINDOW(conduit),cfg);
}

GnomePilotConduit *conduit_get_gpilot_conduit( guint32 pilotId ) 
{
        GObject *retval;
        ConduitCfg *cfg, *cfg2;
        ConduitData *cd = g_new0(ConduitData, 1);

        retval = gnome_pilot_conduit_standard_new("ExpenseDB", Expense_Creator, NULL);
        g_assert(retval != NULL);

        g_signal_connect(retval, "copy_from_pilot", (GCallback)copy_from_pilot ,NULL);
        /*
          g_signal_connect(retval, "copy_to_pilot", (GCallback) ,NULL);
          g_signal_connect(retval, "merge_to_pilot", (GCallback) ,NULL);
          g_signal_connect(retval, "merge_from_pilot", (GCallback) ,NULL);
        */
        g_signal_connect(retval, "synchronize", (GCallback)synchronize ,NULL);
	g_signal_connect (retval, "create_settings_window", (GCallback) create_settings_window, NULL);
	g_signal_connect (retval, "display_settings", (GCallback) display_settings, NULL);
	g_signal_connect (retval, "save_settings", (GCallback) save_settings, NULL);
	g_signal_connect (retval, "revert_settings", (GCallback) revert_settings, NULL);

	load_configuration(&cfg,pilotId);
	cfg2 = dupe_configuration(cfg);
	g_object_set_data(G_OBJECT(retval),OBJ_DATA_CONFIG,cfg);
	g_object_set_data(G_OBJECT(retval),OBJ_DATA_OLDCONFIG,cfg2);
        g_object_set_data(G_OBJECT(retval),OBJ_DATA_CONDUIT,(gpointer)cd);
        
        return GNOME_PILOT_CONDUIT(retval); 
}

void conduit_destroy_gpilot_conduit( GnomePilotConduit *c ) 
{
        ConduitCfg *cc;
        ConduitData  *cd;
  
        cc = GET_CONDUIT_CFG(c);
        cd = GET_CONDUIT_DATA(c);

        destroy_configuration( &cc );
	g_object_unref (G_OBJECT (c));
}

#define OLD_PREFIX ".gnome2/gnome-pilot.d"
#define NEW_PREFIX ".gnome-pilot"

#define IS_STR_SET(x) (x != NULL && x[0] != '\0')

static void
migrate_conf (const gchar *old, const gchar *new)
{
	gchar *basename = g_path_get_dirname (new);

	if (!g_file_test (basename, G_FILE_TEST_EXISTS)) {
		g_mkdir_with_parents (basename, S_IRUSR | S_IWUSR | S_IXUSR);
	} else {
		if (!g_file_test (basename, G_FILE_TEST_IS_DIR)) {
			gchar *tmp = g_strdup_printf ("%s.old", basename);
			rename (basename, tmp);
			g_free (tmp);
			g_mkdir_with_parents (basename, S_IRUSR | S_IWUSR | S_IXUSR);
		}
	}
	g_free (basename);

	if (g_file_test (new, G_FILE_TEST_IS_REGULAR)) {
		return;
	} else if (g_file_test (old, G_FILE_TEST_IS_REGULAR)) {
		rename (old, new);
	} else {
		creat (new, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	}
}

static GKeyFile*
get_kfile (const gchar *conf)
{
	GKeyFile   *kfile = g_key_file_new ();
	const char *homedir = g_getenv ("HOME");
	char       *old = NULL;
	char       *new = NULL;

	if (!homedir)
		homedir = g_get_home_dir ();

	old = g_build_filename (homedir, OLD_PREFIX, conf, NULL);
	new = g_build_filename (homedir, NEW_PREFIX, conf, NULL);

	migrate_conf (old, new);

	g_key_file_load_from_file (kfile, new, G_KEY_FILE_NONE, NULL);
	g_key_file_set_list_separator (kfile, ' ');

	g_free (new);
	g_free (old);
	return kfile;
}

static gboolean
save_config (GKeyFile    *kfile,
	     const gchar *conf)
{
	const char *homedir = g_getenv ("HOME");
        GError     *error = NULL;
        gchar      *data = NULL;
        gsize       size;
	gchar 	   *filename = NULL;

	g_return_val_if_fail (kfile, FALSE);
	g_return_val_if_fail (IS_STR_SET (conf), FALSE);

	if (!homedir)
		homedir = g_get_home_dir ();

	filename = g_build_filename (homedir, NEW_PREFIX, conf, NULL);

	if (! g_file_test (filename, G_FILE_TEST_IS_REGULAR)) {
		g_free (filename);
		g_warning ("File %s does not exsit", filename);
		return FALSE;
	}

        g_message ("Saving config to disk...");

	g_key_file_set_list_separator (kfile, ' ');
 	data = g_key_file_to_data (kfile, &size, &error);
        if (error) {
                g_warning ("Could not get config data to write to file, %s",
                           error->message);
                g_error_free (error);

                return FALSE;
        }

        g_file_set_contents (filename, data, size, &error);
        g_free (data);

        if (error) {
                g_warning ("Could not write %" G_GSIZE_FORMAT " bytes to file '%s', %s",
                           size,
                           filename,
                           error->message);
                g_free (filename);
                g_error_free (error);

                return FALSE;
        }

        g_message ("Wrote config to '%s' (%" G_GSIZE_FORMAT " bytes)",
                   filename,
                   size);

	g_free (filename);

	return TRUE;
}
